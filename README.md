# Lamprey single cell amplicon analysis

This is the main repository for the analysis on the lamprey single-cell
assay data. To run an analysis, clone the repository and add
configuration (tables) for the new data.


## Step-by-step instructions:


### 1. Clone the repository:

```
git clone https://gitlab.com/mpetersen/lamprey-single-cell-amplicons.git
```

You can also specify a new directory as a target. This is useful if you
are cloning the repository for a specific analysis. Just add the new
directory name to the command (the directory must not exist):

```
git clone https://gitlab.com/mpetersen/lamprey-single-cell-amplicons.git New_Analysis_Folder
```


### 2. Modify input tables:

- config/samples.tsv
- config/barcode_primer.tsv
- config/units.tsv
- config/rpis.tsv
- config/config.yaml

In particular, pay attention that the sample identifiers can be
correlated across the tables `samples.tsv`, `units.tsv`, and
`barcode_primer.tsv`. The table names and their locations can be changed
in `config.yaml`. If you run several analyses with a big bunch of
datasets, it might be helpful to know that `units.tsv` and `rpis.tsv`
may contain entries for all datasets. The pipeline will select the
appropriate fastq files and RPI sequences for the samples in
`samples.tsv`.

It is also possible to specify a different configuration file. See below
(step 3) for how. Keep in mind, though, that the pipeline will overwrite
output files with the same name in the **results** directory, therefore
this makes the most sense if you have your config files located
elsewhere.

Refer to the example files that are included in this repository. Make
sure that the files have Unix line endings (LF). This can be an issue if
you create the input tables on a Mac/Windows system and copy them to the
Linux machine that runs the analysis.


### 3. Run the pipeline (on the MPI-IE cluster)

```
module load snakemake
snakemake --jobs 8 --use-conda --cluster 'module load slurm; SlurmEasy -n {rule} -t {threads}'
```

This assumes that all necessary requirements are installed; see the
section "Setup instructions" below.

If you use a different configuration file than **config/config.yaml**
(to specify different sample sheets/barcode tables), you need to tell
Snakemake its location:

```
snakemake --jobs 8 --use-conda --cluster 'module load slurm; SlurmEasy -n {rule} -t {threads}' --configfile path/to/different_config.yaml
```

The config file can contain paths to different tables, for example:

```
samples: /data/boehm/group2/nusser/projects/lamprey-single-cell-amplicons/configs/220404_samples_geneset2.tsv
barcode_primers: /data/boehm/group2/nusser/projects/lamprey-single-cell-amplicons/configs/220404_primers_geneset2.tsv
units: /data/boehm/group2/nusser/projects/lamprey-single-cell-amplicons/configs/220404_all_fastqs.tsv
rpi_sequences: config/rpis.tsv # this can be used every time, it's the default Illumina RPI sequences
```


## Setup instructions

### Install Conda and Mamba

Install the Miniconda installer for Conda. Download the appropriate
package for your system:

https://docs.conda.io/en/latest/miniconda.html

For example, on a (64-bit) Linux system you'll need
[the Linux installer](https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh)

Then follow the [installation
instructions](https://conda.io/projects/conda/en/latest/user-guide/install/linux.html). Basically, run:

```
bash Miniconda3-latest-Linux-x86_64.sh
```

And follow the prompts/questions. You'll have to close and re-open your
terminal window.

Then, install Mamba:

```
conda install -n base -c conda-forge mamba
```

Alternatively, you can skip this step, but then you'll have to tell
Snakemake to use the older Conda instead of Mamba each time with the
`--conda-backend=conda` option.


### If you are running on the MPI-IE cluster

Load the [Snakemake](https://snakemake.readthedocs.io/en/stable/)
module. This way you do not need an extra Snakemake installation.

```
module load snakemake
```

If you are running in a different environment, install Snakemake (for
example, [using
Conda/Mamba](https://snakemake.readthedocs.io/en/stable/getting_started/installation.html)
and make sure you have some kind of cluster resource scheduler such as
SLURM. The following examples use the
[SlurmEasy](https://github.com/dpryan79/Misc/blob/master/MPIIE_internal/SlurmEasy)
script that is installed on the MPI cluster, and loaded with the SLURM
module. If your cluster setup differs, please refer to your HPC
documentation.


## Running the pipeline

```
snakemake --jobs 8 --use-conda
```

This will run the pipeline with 8 parallel jobs, and will make use of
Mamba to install required software packages. However, it will not make
use of the cluster, and run all jobs locally instead (not recommended).
Therefore, add the cluster job submission option similarly to this:

```
snakemake --jobs 8 --use-conda --cluster 'module load slurm; SlurmEasy -n {rule} -t {threads}'
```

Again, this assumes that your cluster has the SLURM module, otherwise
just omit the `module load slurm` part (but make sure that SlurmEasy
works, or use something else in the `--cluster` option).

To avoid having to type `--jobs 8 --use-conda --cluster '...'` every
time, you can create a [cluster configuration
profile](https://snakemake.readthedocs.io/en/stable/executing/cli.html#profiles),
and then just use `--profile <profile name>`.
