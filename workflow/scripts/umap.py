#!/usr/bin/env python3
import os, sys
from pathlib import Path
import numpy as np
import pandas as pd
import scanpy as sc

# adapt for snakemake
if "snakemake" in locals():
    counts_table = str(snakemake.input)
    output = str(snakemake.output)
    filter_min_genes = int(snakemake.params.min_genes)
    filter_min_cells = int(snakemake.params.min_cells)
    figures_dir = str(snakemake.params.figures_dir)
    target_genes = snakemake.params.target_genes
else:
    if len(sys.argv) < 3:
        sys.exit("Fatal: expected arguments: (1) counts table [tsv]; (2) output directory")
    counts_table = sys.argv[1]
    output = sys.argv[2]
    target_genes = sys.argv[3:] # rest of the arguments
    filter_min_genes = 1
    filter_min_cells = 1

results_file = Path(output)

# some general settings
sc.settings.verbosity = 3             # verbosity: errors (0), warnings (1), info (2), hints (3)
sc.settings.figdir = Path(figures_dir) # figures directory
sc.logging.print_header()
sc.settings.set_figure_params(dpi=80, facecolor='white')

# read in the counts data
adata = sc.read_csv(counts_table, delimiter="\t")

adata.var_names_make_unique()  # never hurts
pl_highest_expr_genes = sc.pl.highest_expr_genes(adata, n_top=20, save=".pdf", show = False)

sc.pp.filter_cells(adata, min_genes=filter_min_genes)
sc.pp.filter_genes(adata, min_cells=filter_min_cells)

# categories
adata.var['re_genes'] = adata.var_names.str.startswith(tuple(target_genes))  # annotate the group of genes

# QC
sc.pp.calculate_qc_metrics(adata, qc_vars=['re_genes'], percent_top=None, log1p=False, inplace=True)
pl_violin_pct_counts_re_genes = sc.pl.violin(adata, ['total_counts', 'pct_counts_re_genes'], jitter=0.4, multi_panel=True, save="_pct_counts_re_genes.pdf", show = False)
pl_scatter_pct_counts_re_genes = sc.pl.scatter(adata, x='total_counts', y='pct_counts_re_genes', save="_pct_counts_re_genes.pdf", show = False)
pl_scatter_n_genes_by_counts = sc.pl.scatter(adata, x='total_counts', y='n_genes_by_counts', save="_n_genes_by_counts.pdf", show = False)

# normalise and logarithmise
sc.pp.normalize_total(adata, target_sum=1e4)
sc.pp.log1p(adata)
# this probably doesn't give anything useful with this few genes/cells
sc.pp.highly_variable_genes(adata, min_mean=0.0125, max_mean=3, min_disp=0.5)
# but this is useful
pl_highly_variable_genes = sc.pl.highly_variable_genes(adata, save = ".pdf", show = False)

# save .raw
adata.raw = adata

# scale and center
sc.pp.scale(adata, max_value=10)

# PCA -- omit for now
#sc.tl.pca(adata, svd_solver='arpack')
#sc.pl.pca(adata)
#sc.pl.pca_variance_ratio(adata, log=True, save=".pdf")

# Compute neighbourhood graph
sc.pp.neighbors(adata, n_neighbors=25, n_pcs=40)

# embedding the neighbourhood graph
sc.tl.leiden(adata) # the Leiden algo computes the clusters, a requisite for the following steps
sc.tl.paga(adata) # PAGA
pl_leiden = sc.pl.paga(adata, plot=False)  # remove `plot=False` if you want to see the coarse-grained graph

# UMAP layout and plotting
sc.tl.umap(adata, init_pos='paga')
sc.tl.umap(adata)
pl_umap = sc.pl.umap(adata, color=["leiden"] + target_genes, save="_normalised_logn_counts.pdf", show = False)



# save these results to an object
adata.write(results_file)
