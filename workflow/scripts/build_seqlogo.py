import pandas as pd
import seqlogo

def make_pfm(sequences_df, alphabet = "ACGTN-", alphabet_type = "reduced DNA", background = 0.25):
    max_length = sequences_df.map(str).apply(len).max()
    # add gaps for shorter sequences
    sequences_padded = sequences_df.str.ljust(max_length, "-")
    # count number of nucleotides
    res = dict()
    for letter in alphabet:
        res[letter] = [ (sequences_padded.str[i] == letter).sum() for i in range(max_length)  ]
    res_df = pd.DataFrame(res)
    return seqlogo.CompletePm(res_df, alphabet_type = alphabet_type, background = background)

# def make_seqlogo(pm, filename, format = "svg", ic_scale = True, size = "xlarge):
