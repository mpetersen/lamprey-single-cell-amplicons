import sys
import pandas as pd
from pathlib import Path

if "snakemake" in locals():
    barcode_primers = str(snakemake.input.barcode_primers)
    amplicons_table_file = str(snakemake.input.amplicons_table_file)
    output_file = str(snakemake.output)
    target_genes = snakemake.params.target_genes
else:
    barcode_primers = str(sys.argv[1])
    amplicons_table_file = str(sys.argv[2])
    output_file = str(sys.argv[3])
    target_genes = sys.argv[4:] # rest of the arguments

# read small df
print("reading small table {file}".format(file = barcode_primers))
bcs = pd.read_csv(barcode_primers, sep = "\t")
# normalise column names
bcs.columns = map(str.lower, bcs.columns)
bcs["bc_combo"] = bcs["barcode"].map(str) + "_" + bcs["rpi"].map(str)
bcs["bc_combo"] = bcs["bc_combo"].astype("string")
bcs["cell"] = bcs["plate"].astype(str) + "_" + bcs["rpi"].astype(str) + "_" + bcs["barcode"].astype(str)

# read large df
print("reading large table {file}".format(file = amplicons_table_file))
df = pd.read_csv(amplicons_table_file, sep = "\t").rename(columns = {"barcode": "name"})
df.columns = map(str.lower, df.columns) # normalise column names
df["barcode"] = df["name"].str.extract("([0-9]+)$").astype("int")
df["rpi"] = df["rpi"].str.extract("([0-9]+)$").astype("int")
df["bc_combo"] = df["barcode"].map(str) + "_" + df["rpi"].map(str)
df["bc_combo"] = df["bc_combo"].astype("string")

# merge
print("merging")
jdf = df.merge(bcs, on = "bc_combo",  how = "inner")

# make sure to exclude wrong barcode+read combinations
jdf = jdf.loc[jdf['name_x'] == jdf['name_y']] # this should only affect a few hundred out of more than a million

# extract gene name
genes_string = "^(" + "|".join(target_genes) + ")"
jdf["gene"] = jdf["name_x"].str.extract(genes_string)

# group by cell and gene, and count
print("aggregating")
counts = pd.DataFrame({"count": jdf.groupby(["cell", "gene"])["gene"].count()})

print("writing output")
counts.to_csv(output_file, sep = "\t")

