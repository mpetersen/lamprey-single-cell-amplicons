from Bio import SeqIO
with open(str(snakemake.input), "r") as fa_in, open(str(snakemake.output.fasta), "w") as fa_out:
    n = 0
    for record in SeqIO.parse(fa_in, "fasta"):
        if record.seq.startswith(snakemake.params.motif):
            n += 1
            SeqIO.write(record, fa_out, "fasta")
with open(str(snakemake.output.passed), "w") as count_out:
    count_out.write("{n}\n".format(n = str(n)))

