import sys
import pandas as pd
from pathlib import Path

if "snakemake" in locals():
    amplicon_table_file = str(snakemake.input[0])
    output_directory    = str(snakemake.output[0])
    split_by            = str(snakemake.params["split_by"])
else:
    amplicon_table_file = str(sys.argv[1])
    output_directory    = str(sys.argv[2])
    split_by            = str(sys.argv[3])

# make sure the output directory exists
Path(output_directory).mkdir(parents = True, exist_ok = True)

amplicons = pd.read_csv(amplicon_table_file, delimiter="\t").set_index("barcode_name", drop = False)
amplicons["target"] = amplicons["barcode_name"].str.replace("f_2_bc[0-9]+$", "", regex = True)

amplicons_sliced_dict = { }

if split_by == "target_gene":
    for target in amplicons["target"].unique():
        amplicons_sliced_dict[target] = amplicons[ amplicons["target"] == target ]

    for target in amplicons_sliced_dict:
        amplicons_sliced_dict[target].to_csv(Path(output_directory, target + ".tsv.gz"),
                                           sep = "\t",
                                           index = False,
                                           compression="gzip")
elif split_by == "cell":
    for target in amplicons["barcode_name"].unique():
        amplicons_sliced_dict[target] = amplicons[ amplicons["barcode_name"] == target ]

    for target in amplicons_sliced_dict:
        amplicons_sliced_dict[target].to_csv(Path(output_directory, target + ".tsv.gz"),
                                           sep = "\t",
                                           index = False,
                                           compression="gzip")
else:
    sys.exit("Error: config needs 'split_by' set. Valid options are 'target_gene' or 'cell'."
