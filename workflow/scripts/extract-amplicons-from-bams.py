import sys
import re
import pysam
import csv
import gzip
from Bio import SeqUtils
from Bio import Seq

if "snakemake" in locals():
    barcode_primers = str(snakemake.input.barcode_primers)
    rpi_table_file = str(snakemake.input.rpi_table_file)
    input_bamfile = str(snakemake.input.bam)
    output_file = str(snakemake.output)
    target_genes = snakemake.params.target_genes
else:
    barcode_primers = str(sys.argv[1])
    rpi_table_file = str(sys.argv[2])
    input_bamfile  = str(sys.argv[3])
    output_file = str(sys.argv[4])
    target_genes = sys.argv[5:] # rest of the arguments

def main(barcodes_file, rpi_table_file, bam_file, output_file):

    # read in the barcode primer table, make a dict:
    #      { Name : Sequence }
    def read_barcode_dict(barcode_table_file):
        barcodes = dict()
        with open(barcode_primers, "r") as csvfile:
            reader = csv.DictReader(csvfile, delimiter = "\t")
            reader.fieldnames = [ f.lower() for f in reader.fieldnames ] # normalise column names
            for row in reader:
                barcodes[row["name"]] = row["sequence"]
        return barcodes

    def read_rpi_dict(rpi_table_file):
        rpis = dict()
        with open(rpi_table_file, "r") as csvfile:
            reader = csv.DictReader(csvfile, delimiter = "\t")
            reader.fieldnames = [ f.lower() for f in reader.fieldnames ] # normalise column names
            for row in reader:
                rpis["RPI" + row["rpi"]] = row["rpi_sequence"]
        return rpis

    barcodes = read_barcode_dict(barcodes_file)
    rpis = read_rpi_dict(rpi_table_file)

    def find_barcode(read_pair, barcode_sequence):
        """
        TODO:
        find the barcode sequence in either mate of the pair, reverse
        complement or not
        """


    # parse the bam:
    # get alignments for each each barcode name
    # find the barcode sequence in the alignments
    # output tabular
    def parse_bam(bam_file, barcode_dict, rpi_dict):
        # build read index
        print("opening and indexing bam file")
        sam = pysam.AlignmentFile(bam_file, "rb")
        sam_index = pysam.IndexedReads(sam, multiple_iterators = True)
        sam_index.build()
        rpi_pattern = re.compile("^rpi", re.IGNORECASE)


        # for each mapping, extract the mate as well
        # check whether they match their respective refseqs 100%
        print("parsing bam file")
        data = list()
        n_perfect_matches = 0
        n_no_mate = 0 # count how many reads have no mate
        read_stream = sam.fetch(until_eof = True)
        for read in read_stream:
            if not read.is_paired: continue # skip unpaired reads
            pair = sam_index.find(read.query_name)
            read = next(pair)
            try:
                mate = next(pair)
            # skip reads where the mate didn't map
            except stopiteration:
                n_no_mate += 1
                continue
            # skip reads mapping to the same ref; these can not be assigned to a cell
            if read.reference_name == mate.reference_name: continue
            # check whether each matches their refseqs
            if read.reference_name.startswith(tuple(target_genes)) and rpi_pattern.match(mate.reference_name): # read on barcode, mate on rpi
                barcode_sequence = barcode_dict[read.reference_name]
                rpi_sequence = rpi_dict[mate.reference_name]
            elif rpi_pattern.match(read.reference_name) and mate.reference_name.startswith(tuple(target_genes)): # read on RPI, mate on barcode
                barcode_sequence = barcode_dict[mate.reference_name]
                rpi_sequence = rpi_dict[read.reference_name]
                # swap the reads
                temp = read
                read = mate
                mate = temp
            else:
                continue # read and mate not on barcode and RPI
            # nucleotide sequence search for barcode and rpi
            match_in_reads = find_barcode(pair, barcode_sequence)

            match_on_read = SeqUtils.nt_search(read.query_sequence, barcode_sequence.upper())
            match_on_mate = SeqUtils.nt_search(mate.query_sequence, rpi_sequence.upper())
            # also search in revcomp
            match_on_read_revcomp = SeqUtils.nt_search(Seq.reverse_complement(read.query_sequence), barcode_sequence.upper())
            match_on_mate_revcomp = SeqUtils.nt_search(Seq.reverse_complement(mate.query_sequence), rpi_sequence.upper())

            # both have a match
            # extract subread
            if len(match_on_read) > 1 and len(match_on_mate) > 1:
                n_perfect_matches += 1
                hit_pos = match_on_read[1]
                hit_seq = read.query_sequence[hit_pos:]
            else: # no perfect match
                continue
            data.append( { "reference_name": read.reference_name,
                          "query_name": read.query_name,
                          "read": 1 if read.is_read1 else 2,
                          "strand": "-" if read.is_reverse else "+",
                          "flags": read.flag,
                          "query_seq": read.query_sequence,
                          "query_qual": read.query_qualities,
                          "hit_pos": hit_pos,
                          "hit_seq": hit_seq,
                          "mate_name": mate.query_name,
                          "mate_reference_name": mate.reference_name,
                          "mate_read": 1 if mate.is_read1 else 2,
                          "mate_strand": "-" if mate.is_reverse else "+",
                          "mate_seq": mate.query_sequence,
                          "mate_qual": mate.query_qualities
                        } )
        print("found {n} perfect matches of both barcode and rpi".format(n = n_perfect_matches))
        print("no mate found for {n} reads".format(n = n_no_mate))
        return data

    # run the parser, write output to table
    res = parse_bam(input_bamfile, barcodes, rpis)
    row_format = "{bc}\t{rpi}\t{read_id}\t{flags}\t{read}\t{strand}\t{qseq}\t{hit_pos}\t{hit_seq}\t{mate_read}\t{mate_strand}\t{mate_seq}\n"
    with gzip.open(output_file, "wt") as outfh:
        # header line
        outfh.write(row_format.format(
            bc          = "barcode",
            rpi         = "rpi",
            read_id     = "read_id",
            read        = "read",
            strand      = "strand",
            flags       = "flags",
            qseq        = "read_sequence",
            hit_pos     = "barcode_pos",
            hit_seq     = "amplicon_sequence",
            mate_reference_name   = "mate_reference_name",
            mate_read   = "mate_read",
            mate_strand = "mate_strand",
            mate_seq    = "mate_sequence",
        ))

        for row in res:
            outfh.write(row_format.format(
                bc          = row["reference_name"],
                rpi         = row["mate_reference_name"],
                read_id     = row["query_name"],
                flags       = row["flags"],
                read        = row["read"],
                strand      = row["strand"],
                qseq        = row["query_seq"],
                hit_pos     = row["hit_pos"],
                hit_seq     = row["hit_seq"],
                mate_read   = row["mate_read"],
                mate_strand = row["mate_strand"],
                mate_seq    = row["mate_seq"],
            ))

if __name__ == "__main__":
    main(barcode_primers, rpi_table_file, input_bamfile, output_file)
