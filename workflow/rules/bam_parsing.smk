localrules: extract_amplicons
rule extract_amplicons:
    input:
        barcode_primers = config["barcode_primers"],
        rpi_table_file = config["rpi_sequences"],
        bam = "results/020_filtered_sorted_bam/{sample}.bam"
    output:
        "results/030_extracted_amplicons/{sample}.tsv.gz"
    conda:
        "../envs/biopython.yaml"
    params:
        target_genes = target_genes
    script:
        "../scripts/extract-amplicons-from-bams.py"


localrules: concatenate_amplicon_table
rule concatenate_amplicon_table:
    input:
        expand("results/030_extracted_amplicons/{sample}.tsv.gz", sample = samples["sample"])
    output:
        "results/040_concatenated_amplicon_table/all_samples.tsv.gz"
    run:
        import pandas as pd
        long_df = pd.DataFrame()
        for infile in input:
            df = pd.read_csv(infile, compression = "gzip", sep = "\t")
            long_df = pd.concat([long_df, df])
        long_df.to_csv(output[0], compression = "gzip", sep = "\t", index = False)


def collect_sliced_amplicons(wildcards):
    checkpoint_output = checkpoints.slice_amplicons.get(**wildcards).output[0]
    files = expand("results/050_extracted_amplicons_sliced/{config_split_by}/{target}.tsv.gz",
                   config_split_by = config["split_by"],
                   target = glob_wildcards(os.path.join(checkpoint_output, "{target}.tsv.gz")).target)
    print(files)
    return files


# this is not used currently
rule all_sliced_amplicons:
    input:
        collect_sliced_amplicons


# this is not used currently
localrules: tab2fasta
rule tab2fasta:
    input:
        "results/050_extracted_amplicons_sliced/{config_split_by}/{target}.tsv.gz"
    output:
        "results/060_extracted_amplicons_sliced_as_fasta/{config_split_by}/{target}.fasta"
    params:
        tab2fasta_script = "workflow/scripts/tab2fasta.awk"
    shell:
        """
        zcat {input} \
        | awk -f {params.tab2fasta_script} > {output}
        """


# this is also not used currently
localrules: slice_amplicons_by_cells
checkpoint slice_amplicons_by_cells:
    input:
        "results/040_extracted_amplicons/amplicons.tsv.gz"
    output:
        directory("results/050_extracted_amplicons_sliced/{config_split_by}".format(config_split_by = config["split_by"]))
    params:
        split_by = config["split_by"]
    script:
        "../scripts/slice_amplicon_table.py"
