rule spades:
    input:
        "results/080_non-redundant_amplicons_filtered/{config_split_by}/{target}.cluster_rep_seq_filtered.fasta"
    output:
        "results/090_assembled_amplicons/{config_split_by}/{target}_spades_contigs.fasta"
    conda:
        "../envs/spades.yaml"
    params:
        k = 127
    threads:
        24
    shell:
        """
        spades.py --rna -t {threads} -k {params.k} -s {input} -o $(dirname {output})/{wildcards.target}_spades
        ln $(dirname {output})/{wildcards.target}_spades/K{params.k}/final_contigs.fasta {output}
        """

localrules: filter_assembly
rule filter_assembly:
    input:
        "results/090_assembled_amplicons/{config_split_by}/{target}_spades_contigs.fasta"
    output:
        fasta = "results/100_assembled_amplicons_filtered/{config_split_by}/{target}_spades_contigs_filtered.fasta",
        passed = "results/100_assembled_amplicons_filtered/{config_split_by}/{target}_spades_contigs_filtered.passed"
    conda:
        "../envs/biopython.yaml"
    params:
        motif = config["rp1_primer"]
    script:
        "../scripts/filter-by-motif.py"

def all_assemblies(wildcards):
    targets = glob_wildcards("results/100_assembled_amplicons_filtered/" + config["split_by"] + "/{target}_spades_contigs_filtered.fasta").target
    fastas = expand("results/100_assembled_amplicons_filtered/{config_split_by}/{target}_spades_contigs_filtered.fasta", config_split_by = config["split_by"], target = targets)
    return fastas

localrules: assembly_stats
rule assembly_stats:
    input: all_assemblies
    output:
        "results/110_assembly_statistics/stats.tsv"
    conda:
        "../envs/seqkit.yaml"
    shell:
        """
        seqkit stats -bT {input} > {output}
        """
