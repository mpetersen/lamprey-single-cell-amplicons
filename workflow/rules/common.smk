import pandas as pd
import datetime
import os
import sys

configfile: "config/config.yaml"

samples         = pd.read_csv(config["samples"],         sep = "\t", dtype = str, comment = "#").set_index("sample", drop = False)
units           = pd.read_csv(config["units"],           sep = "\t", dtype = str, comment = "#").set_index("sample", drop = False)
barcode_primers = pd.read_csv(config["barcode_primers"], sep = "\t", dtype = str, comment = "#").set_index("Name", drop = False)
rpi_seqs        = pd.read_csv(config["rpi_sequences"],   sep = "\t", dtype = str, comment = "#")

# normalise column names
samples.columns         = map(str.lower, samples.columns)
units.columns           = map(str.lower, units.columns)
barcode_primers.columns = map(str.lower, barcode_primers.columns)
rpi_seqs.columns        = map(str.lower, rpi_seqs.columns)

# extract some lists and a dictionary
target_genes      = samples["target"].unique().tolist()
cell_barcodes     = barcode_primers["name"].unique().tolist()
barcode_name2gene = { key: re.sub("f_2_bc[0-9]+$", "", key) for key in cell_barcodes }

if config["split_by"] == "cells":
    targets = cell_barcodes
else:
    targets = target_genes

# merge on RPI number to include RPI sequence
barcode_primers = pd.merge(barcode_primers, rpi_seqs, on = "rpi")


def get_fastqs(wildcards):
    """Get raw FASTQ files from unit sheet."""
    files = units.loc[ wildcards.sample, [ "r1", "r2" ] ]
    return files.tolist()


