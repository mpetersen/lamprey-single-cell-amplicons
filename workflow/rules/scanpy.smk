localrules: umap
rule umap:
    input:
        "results/050_read_counts_per_cell/counts_by_cell.tsv"
    output:
        "results/060_scanpy/scanpy_results.h5f"
    conda:
        "../envs/scanpy.yaml"
    params:
        min_genes = config["filter_min_genes"],
        min_cells = config["filter_min_cells"],
        figures_dir = "results/figures",
        target_genes = target_genes
    script:
        "../scripts/umap.py"
