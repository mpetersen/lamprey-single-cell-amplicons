# this step collapses identical reads
rule collapse_identical:
    input:
        "results/060_extracted_amplicons_sliced_as_fasta/{config_split_by}/{target}.fasta"
    output:
        "results/070_non-redundant_amplicons/{config_split_by}/{target}.cluster_rep_seq.fasta"
    conda:
        "../envs/clustering.yaml"
    threads:
        12
    params:
        min_seq_id = 1.0,
        cov_mode = 1,
        cov_fraction = 0.1,
        outdir = "results/060_non-redundant_amplicons",
        tmpdir = config["tmpdir"]
    shell: # TODO target-specific output
        """
        mmseqs easy-linclust \
            --threads {threads} \
            --min-seq-id {params.min_seq_id} \
            --cov-mode {params.cov_mode} \
            -c {params.cov_fraction} \
            {input} \
            $(dirname {output})/{wildcards.target}.cluster \
            {params.tmpdir}
        """

# extract those with the target motif
localrules: filter_reads
rule filter_reads:
    input:
        "results/070_non-redundant_amplicons/{config_split_by}/{target}.cluster_rep_seq.fasta"
    output:
        fasta = "results/080_non-redundant_amplicons_filtered/{config_split_by}/{target}.cluster_rep_seq_filtered.fasta",
        passed = "results/080_non-redundant_amplicons_filtered/{config_split_by}/{target}.cluster_rep_seq_filtered.passed"
    conda:
        "../envs/biopython.yaml"
    params:
        motif = lambda w: config["exon_motifs"][barcode_name2gene[w.target]]
    script:
        "../scripts/filter-by-motif.py"

