localrules: count_reads_per_cell
rule count_reads_per_cell:
    input:
        amplicons_table_file = "results/040_concatenated_amplicon_table/all_samples.tsv.gz",
        barcode_primers = config["barcode_primers"]
    output:
        "results/050_read_counts_per_cell/counts.tsv"
    params:
        target_genes = target_genes
    script:
        "../scripts/count-reads-per-cell.py"

localrules: pivot_wider
rule pivot_wider:
    input:
        "results/050_read_counts_per_cell/counts.tsv"
    output:
        "results/050_read_counts_per_cell/counts_by_cell.tsv"
    run:
        import pandas as pd
        df = pd.read_csv(str(input), sep = "\t")
        df_wide = df.pivot_table(index = "cell", columns = "gene", values = "count").fillna(0)
        df_wide.to_csv(str(output), sep = "\t")
