localrules: make_bwa_index
rule make_bwa_index:
    input:
        rpis = config["rpi_sequences"],
        barcodes = config["barcode_primers"]
    output:
        index = config["barcode_primers_rpi_fasta"],
        ok = "data/ref/index.ok"
    log:
        "logs/bwa/bwa_index.log"
    conda:
        "../envs/bwa_samtools.yaml"
    shell:
        """
        cut -f1,2 {input.barcodes} \
        | tail -n +2 \
        | sort \
        | uniq \
        | awk '{{ printf(">%s\\n%s\\n", $1, $2) }}' \
        > {output.index}

        cut -f1,2 {input.rpis} \
        | tail -n +2 \
        | sort \
        | uniq \
        | awk '{{ if (match($1, "^RPI")) {{ lead = "" }} else {{ lead = "RPI" }} printf(">%s%s\\n%s\\n", lead, $1, $2) }}' \
        >> {output.index}

        bwa index {output.index} > {log}

        touch {output.ok}
        """


rule bwa:
    input:
        fastqs = get_fastqs,
        index = config["barcode_primers_rpi_fasta"]
    output:
        "results/010_bwa/{sample}.bam"
    log:
        "logs/bwa/{sample}.bwa.log"
    threads:
        config["bwa_threads"]
    params:
        seedlength = "-k " + str(config["min_alignment_length"]),
        other = config["bwa_mem_options"],
    conda:
        "../envs/bwa_samtools.yaml"
    shell:
        """
        bwa mem -t {threads} {params.seedlength} {params.other} {input.index} {input.fastqs[0]} {input.fastqs[1]} \
        | samtools view -bh > {output}
        """



localrules: filter_sort_index_bam
rule filter_sort_index_bam:
    input:
        "results/010_bwa/{sample}.bam"
    output:
        "results/020_filtered_sorted_bam/{sample}.bam"
    threads:
        config["samtools_threads"]
    conda:
        "../envs/bwa_samtools.yaml"
    params:
        min_quality = 0,
        other = ""
    shell:
        """
        samtools view -h -q {params.min_quality} -F 12 {params.other} {input} \
        | samtools view -h -F 2048 \
        | samtools sort -@ {threads} - \
        > {output}
        samtools index {output}
        """


localrules: concatenate_bams
rule concatenate_bams:
    input:
        expand("results/020_filtered_sorted_bam/{sample}.bam", sample = samples["sample"])
    output:
        "results/030_concatenated_bam/all_samples.bam"
    threads:
        config["samtools_threads"]
    conda:
        "../envs/bwa_samtools.yaml"
    params:
        other = ""
    shell:
        """
        samtools cat {input} | samtools sort -@ {threads} - > {output}
        samtools index {output}
        """
